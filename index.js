const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');

const {
  authMiddleware,
  checkRole,
} = require('./routers/middlewares/authMiddleware');

const PORT = process.env.PORT ? process.env.PORT : 8080;

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter.js');

const DRIVER = 'DRIVER';

const app = express();

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth/', authRouter);
app.use('/api/users/', authMiddleware, usersRouter);
app.use('/api/trucks/', authMiddleware, checkRole(DRIVER), trucksRouter);
app.use('/api/loads/', authMiddleware, loadsRouter);

const start = async () => {
  await mongoose.connect('mongodb+srv://oleksandra:sasha20@cluster0.g8wwf.mongodb.net/hw2-crud-api?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();
