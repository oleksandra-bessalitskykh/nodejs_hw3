const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

const state = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];

module.exports.getLoads = async (req, res) => {
  if (req.user.role === 'SHIPPER') {
    const loads = await Load.find({created_by: req.user._id});
    res.json({loads});
  }
  if (req.user.role === 'DRIVER') {
    const loads = await Load.find({assigned_to: req.user._id});
    res.json({loads});
  }
};

module.exports.postLoads = async (req, res) => {
  const load = new Load({
    name: req.body.name,
    payload: req.body.payload,
    pickup_address: req.body.pickup_address,
    delivery_address: req.body.delivery_address,
    dimensions: {
      width: req.body.dimensions.width,
      length: req.body.dimensions.length,
      height: req.body.dimensions.height,
    },
    created_by: req.user._id,
    assigned_to: null,
    status: 'NEW',
    state: 'En route to Pick Up',
    logs: [],
    created_date: Date.now()
  });
  await load.save();
  res.json({message: 'Load created successfully'});
};

module.exports.getActiveLoad = async (req, res) => {
  const load = await Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'});
  res.json({load});
};

module.exports.patchLoad = async (req, res) => {
  const load = await Load.findOne({assigned_to: req.user._id, status: 'ASSIGNED'});
  let stateIndex = state.findIndex(state => state === load.state);
  const newState = state[stateIndex + 1];
  if (stateIndex < 2) {
    await Load.findByIdAndUpdate(load._id, {state: newState})
  } else {
    await Load.findByIdAndUpdate(load._id, {state: newState, status: 'SHIPPED'})
  }
  res.json({message: `Load state changed to '${newState}'`});
};

module.exports.getLoad = async (req, res) => {
  if (req.user.role === 'SHIPPER') {
    const load = await Load.findOne({_id: req.params.id, created_by: req.user._id});
    res.json({load});
  }
  if (req.user.role === 'DRIVER') {
    const load = await Load.findOne({_id: req.params.id, assigned_to: req.user._id});
    res.json({load});
  }
};

module.exports.putLoad = async (req, res) => {
  const load = await Load.findById(req.params.id);
  if (load.status !== 'NEW') {
    res.status(400).json({message: 'Bad request!'})
  }
  await Load.findByIdAndUpdate(req.params.id, {
    name: req.body.name,
    payload: req.body.payload,
    pickup_address: req.body.pickup_address,
    delivery_address: req.body.delivery_address,
    dimensions: {
      width: req.body.dimensions.width,
      length: req.body.dimensions.length,
      height: req.body.dimensions.height,
    }
  });
  res.json({message: 'Load edited successfully'});
};

module.exports.deleteLoad = async (req, res) => {
  const load = await Load.findById(req.params.id);
  if (load.status !== 'NEW') {
    res.status(400).json({message: 'Bad request!'})
  } else {
    await Load.findByIdAndDelete(req.params.id);
    res.json({message: 'Load deleted successfully'});
  }
};

module.exports.postLoad = async (req, res) => {
  await Load.findByIdAndUpdate(req.params.id, {status: 'POSTED'});
  const truck = await Truck.findOne({assigned_to: {$ne: null}});
  if (truck) {
    await Load.findByIdAndUpdate(req.params.id, {status: 'ASSIGNED', assigned_to: truck.assigned_to});
    res.json({message: 'Load posted successfully', driver_found: true});
  } else {
    await Load.findByIdAndUpdate(req.params.id, {status: 'NEW'});
    res.json({message: 'Load posted successfully', driver_found: false});
  }
};

module.exports.getLoadShippingInfo = async (req, res) => {
  const load = await Load.findById(req.params.id);
  const driver = await User.findById(load.assigned_to);
  const truck = await Truck.findOne({assigned_to: driver._id});
  res.json({load, truck});
};