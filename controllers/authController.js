const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const {Credential} = require('../models/credentialsModel');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;

  const credentials = new Credential({
    email,
    password: await bcrypt.hash(password, 10),
  });

  const user = new User({
    email, role,
  });

  await credentials.save();

  await user.save();

  res.json({message: 'Success'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const credentials = await Credential.findOne({email});
  const user = await User.findOne({email});

  if (!credentials) {
    return res.status(400).json({message: `No user with email '${email}' found!`});
  }

  if (!(await bcrypt.compare(password, credentials.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({email: user.email, _id: user._id, role: user.role}, JWT_SECRET);
  res.json({message: 'Success', jwt_token: token});
};
