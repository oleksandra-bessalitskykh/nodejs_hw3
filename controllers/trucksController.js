const {Truck} = require('../models/truckModel');

module.exports.getTrucks = async (req, res) => {
  const trucks = await Truck.find({created_by: req.user._id});
  res.json({trucks});
};

module.exports.postTrucks = async (req, res) => {
  const truck = new Truck({
    created_by: req.user._id,
    assigned_to: null,
    status: 'IS',
    created_date: Date.now(),
    type: req.body.type
  });
  await truck.save();
  res.json({message: 'Truck created successfully'});
};

module.exports.getTruck = async (req, res) => {
  const truck = await Truck.findById(req.params.id);
  res.json({truck});
};

module.exports.putTruck = async (req, res) => {
  const truck = await Truck.findById(req.params.id);

  if (!truck.assigned_to) {
    res.status(400).json({message: `Driver is able to update only not assigned to him trucks info!`});
  } else {
    await Truck.findByIdAndUpdate(req.params.id, {type: req.body.type});
    res.json({message: 'Truck details changed successfully'});
  }
};

module.exports.deleteTruck = async (req, res) => {
  const truck = await Truck.findById(req.params.id);

  if (!truck.assigned_to) {
    res.status(400).json({message: `Driver is able to delete only not assigned to him trucks info!`});
  } else {
    await Truck.findByIdAndDelete(req.params.id);
    res.json({message: 'Truck deleted successfully'});
  }
};

module.exports.postTruck = async (req, res) => {
  const truck = await Truck.findById(req.params.id);

  if (truck.assigned_to) {
    res.status(400).json({message: `Driver is able to assign only not assigned to him trucks info!`});
  } else {
    await Truck.findByIdAndUpdate(req.params.id, {assigned_to: req.user._id});
    res.json({message: 'Truck assigned successfully'});
  }
};