const express = require('express');
const trucksRouter = express.Router();

const {asyncWrapper} = require('./helpers');
const {
  getTrucks,
  postTrucks,
  getTruck,
  putTruck,
  deleteTruck,
  postTruck,
} = require('../controllers/trucksController');

trucksRouter.get('/', asyncWrapper(getTrucks));
trucksRouter.post('/', asyncWrapper(postTrucks));
trucksRouter.get('/:id', asyncWrapper(getTruck));
trucksRouter.put('/:id', asyncWrapper(putTruck));
trucksRouter.delete('/:id', asyncWrapper(deleteTruck));
trucksRouter.post('/:id/assign', asyncWrapper(postTruck));

module.exports = trucksRouter;