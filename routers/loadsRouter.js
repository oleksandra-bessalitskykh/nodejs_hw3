const express = require('express');
const loadsRouter = express.Router();

const {asyncWrapper} = require('./helpers');
// const {validateNote} = require('./middlewares/validationMiddleware');
const {
  getLoads,
  postLoads,
  getActiveLoad,
  patchLoad,
  getLoad,
  putLoad,
  deleteLoad,
  postLoad,
  getLoadShippingInfo
} = require('../controllers/loadsController');

const SHIPPER = "SHIPPER";
const DRIVER = "DRIVER";

const {checkRole} = require('./middlewares/authMiddleware');

loadsRouter.get('/', asyncWrapper(getLoads));

loadsRouter.post('/', checkRole(SHIPPER), asyncWrapper(postLoads));

loadsRouter.get('/active', checkRole(DRIVER), asyncWrapper(getActiveLoad));

loadsRouter.patch('/active/state', checkRole(DRIVER), asyncWrapper(patchLoad));

loadsRouter.get('/:id', asyncWrapper(getLoad));

loadsRouter.put('/:id', checkRole(SHIPPER), asyncWrapper(putLoad));
loadsRouter.delete('/:id', checkRole(SHIPPER), asyncWrapper(deleteLoad));
loadsRouter.post('/:id/post', checkRole(SHIPPER), asyncWrapper(postLoad));
loadsRouter.get('/:id/shipping_info', checkRole(SHIPPER), asyncWrapper(getLoadShippingInfo));

module.exports = loadsRouter;