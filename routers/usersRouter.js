const express = require('express');
const authRouter = express.Router();

const {asyncWrapper} = require('./helpers');
const {validatePassword} = require('./middlewares/validationMiddleware');
const {getUser, deleteUser, patchUser} = require('../controllers/usersController');

authRouter.get('/me', asyncWrapper(getUser));
authRouter.delete('/me', asyncWrapper(deleteUser));
authRouter.patch('/me', asyncWrapper(validatePassword), asyncWrapper(patchUser));

module.exports = authRouter;
