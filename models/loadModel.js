const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  name: String,
  payload: Number,
  pickup_address: String,
  delivery_address: String,
  dimensions: {
    width: Number,
    length: Number,
    height: Number
  },
  created_by: String,
  assigned_to: String,
  status: String,
  logs: [
    {
      message: String,
      time: String,
    }
  ],
  created_date: String
});

module.exports.Load = mongoose.model('Load', loadSchema);