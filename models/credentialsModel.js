const mongoose = require('mongoose');

const credentialSchema = mongoose.Schema({
  email: {
    type: String,
    uniq: true
  },
  password: String
});

module.exports.Credential = mongoose.model('Credential', credentialSchema);