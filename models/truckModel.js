const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  type: String,
  created_by: String,
  assigned_to: String,
  status: String,
  created_date: String
});

module.exports.Truck = mongoose.model('Truck', truckSchema);