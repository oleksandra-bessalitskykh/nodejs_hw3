const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  email: {
    type: String,
    uniq: true
  },
  password: String,
  created_date: {
    type: String,
    default: Date.now()
  },
  role: String
});

module.exports.User = mongoose.model('User', userSchema);